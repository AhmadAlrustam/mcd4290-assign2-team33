// Shared code needed by the code of all three pages.

// Prefix to use for Local Storage.  You may change this.
var APP_PREFIX = "monash.eng1003.fencingApp";

//Show the path of the positions marked
var routePath = null;

//Show the polygon of the area surrounded by the positions marked
var polygon = null;

//Check if the local storage can be used.If not jump a window to inform the user.
if (typeof(Storage) === "undefined") {
    displayMessage("localStorage is not supported by current browser.");
}

//Create class Region, consisting of the properties if position and time
class Region {
    constructor() {
        this.cornerPosition = [];
        this.timeReference = null;
        this.numOfFenceposts = null;
    }

    //setter to set marker positions/ latlng object into private variable corner position
    setPosition = function(paraP) {
            this.cornerPosition.push(paraP);
        }
        //getter to get corner position
    getPosition = function() {
            return this.cornerPosition;
        }
        //setter to set time saved when save() function is called
    setTime = function(paraT) {
            this.timeReference = paraT;
        }
        //getter to get time saved
    getTime = function() {
            return this.timeReference;
        }
        //setter to set the number of the fenceposts when save() function is called
    setNumOfFenceposts = function(paraNum) {
            this.numOfFenceposts = paraNum;
        }
        //getter to get the number of the fenceposts
    getNumOfFenceposts = function() {
        return this.numOfFenceposts;
    }
}
var saveData = new Region(); //Create a new class instance of the Region


//This function can show the path between the corner.
function showPath() {
    //If it is already shown on the map, don't show the ploygon again.
    if (polygon !== null) {
        polygon.setMap(null);
    }
    routePath = new google.maps.Polyline({
        //The path is based on saveData.
        path: saveData.getPosition(),
        geodesic: true,
        //Set the weight
        strokeWeight: 2,
        //Set the color
        strokeColor: '#0000FF',
        //Set the transparency
        strokeOpacity: 1.0
    });
    routePath.setMap(map); //show path on the map
}


//This function can show the polygon.
function showPolygon() {
    //If it is already shown on the map, don't show the path again.
    if (routePath !== null) {
        routePath.setMap(null);
    }
    polygon = new google.maps.Polygon({
        //The polygon path is based on saveData.
        paths: saveData.getPosition(),
        //Set the stroke color
        strokeColor: '#A500CC',
        //Set the transparency
        fillOpacity: 0.35,
        //Set the weight.
        strokeWeight: 2,
        fillColor: '#E93EFF'
    });
    polygon.setMap(map); //Show polygon on the map
}

//Create class RegionList, includes the necessary attributes 
class RegionList {
    constructor() {
        this.regions = []; // Array of regions 
        this.number_of_regions = 0; //number of regions
    }

    add_region(region) {
        this.regions.push(region);

    }

    remove_region(index) {
        this.regions.splice(index, 1);
    }

    get_region(index) {
        return this.regions[index];

    }

    get_number_of_regions() {
        return this.regions.length;
    }

}