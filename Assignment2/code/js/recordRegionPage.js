// Code for the Record Region page.
//Create a new variable called map and set the initial value to null.Then use the function called
//initMap to display the google map on the viewRegion page
var map = null;
var blueIcon = 'http://www.robotwoods.com/dev/misc/bluecircle.png';

//Create two private variables(array) to store the position from the local storage and save them back to the local storage in case of the rewrite when the page is reloading.
var arrayOfPos = [];

//Create three position related variables and use the in-built function called watch position to get the
//current location
var latitudeValue, longitudeValue, accuracyValue;

//Create two new empty arrays to store two class instances called "userMarker" and "accuracyCircle" 
//respectively.When calling the function in the following function called showCurrentLocation(),it will
//store one element into each array.
var userMarkers = [];
var accuracyCircles = [];

//Put an initial map on the web page
function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
        //Map center position
        center: {
            lat: -37.9118667,
            lng: 145.1329004
        },
        zoom: 19
    });

    geocoder = new google.maps.Geocoder();

    google.maps.event.addListener(map, 'click', function(event) {
        var Latitude = event.latLng.lat().toFixed(4);
        var longitude = event.latLng.lng().toFixed(4);
        placeMarker(event.latLng, Latitude, longitude);
    });

    /*google.maps.event.addListener(marker, "rightclick", function() { //right click to remove the maker
        deleteMakers();
    });*/

}

//Check if the local storage can be used.If not, jump a window to let user know.
if (typeof(Storage) === "undefined") {
    displayMessage("localStorage is not supported by current browser.");
}
//Create a function called addCorner, you can drop the corner at your current position
//When click the botton "save" in the recordRegion page,
//it will save the current location into local storage for future reference
function addCorner() {
    map.panTo({
        //Pan to this position at first, make sure it is current position.
        lat: latitudeValue,
        lng: longitudeValue
    })

    marker = new google.maps.Marker({ //Set an corner of the current position on the page when clicking the addCorner botton
        map: map,
        draggable: false,
        //The corner will drop on the map
        animation: google.maps.Animation.DROP,
        //The corner position
        position: {
            lat: latitudeValue,
            lng: longitudeValue
        }
    })
    saveData.setPosition(marker.position); //put the position into the class instance.
}


function save() {
    //If you click save botton before addCorner botton, it will jump a window to let user know.
    if (saveData.getPosition().length <= 2) {
        displayMessage("There are less than 3 corners and cannot be saved!");
    } else {
        //If there are some positions in the class instance,it will store the location into the local storage using the time to be the key.
        //save time in
        saveData.setTime(new Date());
        //get the position from the saveData and save it in tempData
        var tempData = saveData.getPosition();
        //Push the starting point into the end of the array to form a closed area
        tempData.push(tempData[0]);
        localStorage.setItem(new Date(), JSON.stringify(tempData));
        location.href = 'index.html'; //Navigate the user to go back to the index page
    }

}


if (navigator.geolocation) {
    positionOptions = {
        enableHighAccuracy: true,
        timeout: Infinity,
        maximumAge: 0
    };
    navigator.geolocation.watchPosition(showCurrentLocation, errorHandler, positionOptions);
}

//If current location has error, it will display.
function errorHandler(error) {
    if (error.code == 0) {
        displayMessage("Unknown error");
    }
    if (error.code == 1) {
        displayMessage("Access denied by user");
    }

    if (error.code == 2) {
        displayMessage("Position unavailable");
    }

    if (error.code == 3) {
        displayMessage("Timed out");
    }
}

//Creat a function to add the marker "Blue icon", it shows current location
function addMarker() {
    var userMarker = new google.maps.Marker({
        //Marker's position(lat, lng)
        position: {
            lat: latitudeValue,
            lng: longitudeValue
        },
        map: map,
        //Marker's icon
        icon: blueIcon
    });
    //Accuracy range for your current location
    var accuracyCircle = new google.maps.Circle({
        //The center of accuracy range, it is your current location but not precise.
        center: {
            lat: latitudeValue,
            lng: longitudeValue
        },
        //Radius of accuracy range
        radius: accuracyValue,
        //Accuracy range transparency
        fillOpacity: 0.35,
        //Accuracy range color
        fillColor: '#1E90FF',
        strokeColor: '#1E90FF',
        //Accuracy range weight
        strokeWeight: 2,
        //Accuracy range stroke transparency 
        strokeOpacity: 0.3,
        //Can not move
        draggable: false,
        map: map
    });
    //Push them to userMarkers array.
    userMarkers.push(userMarker);
    //Push them to caauracyCircles array.
    accuracyCircles.push(accuracyCircle);
}

// Sets the map on all markers in the array.
function setMapOnAll(map) {
    for (var i = 0; i < userMarkers.length; i++) {
        //Use setMap() put userMarkers[i] into the array. 
        userMarkers[i].setMap(map);
        //Use setMap() put accuracyCircles[i] into the array. 
        accuracyCircles[i].setMap(map);
    }
}

// Deletes all markers in the array by removing references to them.
function deleteMarkers() {
    //removing markers and circles, only show the current location.
    setMapOnAll(null);
    userMarkers = [];
    accuracyCircles = [];
}

//This function will keep the current position as the latest state.
function showCurrentLocation(position) {
    //Get latitude, longitude and accuracy
    latitudeValue = Number(Number(position.coords.latitude).toFixed(4));
    longitudeValue = Number(Number(position.coords.longitude).toFixed(4));
    accuracyValue = Number(Number(position.coords.accuracy).toFixed(2));
    //panTo the current position
    map.panTo({
        lat: latitudeValue,
        lng: longitudeValue
    })

    //delete the last marker
    deleteMarkers();
    //put the marker on the current location
    addMarker();

    //If the accuracyValue is greater than 100m, then it will let user know.
    if (accuracyValue > 100) {
        displayMessage("It is not accurate enough to get your current position");
    }
}

//Click the screen to get the position and place the corner.
function placeMarker(location, Latitude, longitude) {

    marker = new google.maps.Marker({
        map: map,
        draggable: false,
        //The marker will drop on the map
        animation: google.maps.Animation.DROP,
        position: location
    })

    var MarkerPos = {
        position: {
            lat: latitudeValue,
            lng: longitudeValue
        }
    }

    MarkerPos.position.lat = Number(Latitude);
    MarkerPos.position.lng = Number(longitude);
    saveData.setPosition(MarkerPos.position);
    //Put the Lat and Lng into the saveData.


}