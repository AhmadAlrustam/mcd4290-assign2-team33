// Code for the View Region page.
var settingKey = "Settinng_Key"; //Setting sessionStorage key
// The following is sample code to demonstrate navigation.
// Youneed not use it for final app.
var regionIndex = localStorage.getItem(APP_PREFIX + "-selectedRegion");
//Set the variable map null at the beginning
var map = null;
//Define two variable position1,position2 to store the positions in the form of LatLng
var p1, p2;
//Set answerLengthRef and answerAreaRef to get the element from html using Id
//Set a variable 'numOfRegion' is to find out which page in the index does the use click
var answerLengthRef, answerAreaRef, numOfRegion;
var currentSaveData, area;
//Define area and length to store the value of length and area(give initial value of length to 0)
var length = 0;

//Calculate how many fences
var line = null;
//Before click, give an initial value 0 to the number of fence
var numFenceposts = 0;
var fenceNumRef;
var answerNumFence;

//Check if the local storage can be used.If not, jump a window to let user know.
if (typeof(Storage) === "undefined") {
    displayMessage("localStorage is not supported by current browser.");
}

//Put an original map in the view region page
function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
        //Set the map center
        center: {
            lat: -37.9118667,
            lng: 145.1329004
        },
        zoom: 19,
        mapTypeId: 'terrain'
    });

    //Set up and empty array to save the position in the form of LatLng
    var myNewLocation = [];
    //Show path and polygon when the user open the viewRegion page
    var routePath = null;
    var polygon = null;
    //Get the Lat and Lng value from the currentSaveData and calculate the average value
    var tempLat = 0;
    var tempLng = 0;


    //Find the region User click 
    numOfRegion = sessionStorage.getItem("Clicking"); //get the index of the user click map
    //Get the position data in region from the local storage
    currentSaveData = JSON.parse(localStorage.getItem(localStorage.key(Number(numOfRegion))));
    //Display the length.
    answerLengthRef = document.getElementById('answerLength');

    //Calculate the distance between two corners.
    for (var i = 0; i < currentSaveData.length - 1; i++) {
        //Use google.maps.Latlng class to assign position1 and position2 to the form of latitude and longitude
        //Add all corners up using google built-in method
        var position1 = new google.maps.LatLng(currentSaveData[i]);
        var position2 = new google.maps.LatLng(currentSaveData[i + 1]);
        //Sum all the distance between each two corners.
        length += Number(google.maps.geometry.spherical.computeDistanceBetween(position1, position2));
    }

    //Put the final length in the corresponding div 
    answerLengthRef.innerHTML = length;
    //Put answerArea to nserAreaRef.
    answerAreaRef = document.getElementById("answerArea");


    //Calculate the area surrounded by markers using google built-in method 
    for (var j = 0; j < currentSaveData.length; j++) {
        //Set two temporary variable to get position and change the form
        var tempPos1 = currentSaveData[j];
        var tempPos2 = new google.maps.LatLng(tempPos1);
        //Then push it into the empty array
        myNewLocation.push(tempPos2);
    }
    //Put the emtire array into the in bulit function to calculate the area.
    area = google.maps.geometry.spherical.computeArea(myNewLocation);
    //Put the correct area in the corresponding div
    answerAreaRef.innerHTML = area;

    //This is the path show in viewRegionPage
    routePath = new google.maps.Polyline({
        //The path is based on currentSaveData
        path: currentSaveData,
        //Let the path become geodesic.
        geodesic: true,
        //The color setting
        strokeColor: '#0000FF',
        //Transparency setting
        strokeOpacity: 1.0,
        //Weight setting
        strokeWeight: 2
    });
    routePath.setMap(map);

    //This is the polygon show in viewRegionPage.
    polygon = new google.maps.Polygon({
        //The path is based on currentSaveData
        paths: currentSaveData,
        //Set the polygon stroke color
        strokeColor: '#A500CC',
        //Set the stroke weight
        strokeWeight: 2,
        //Set polygon color
        fillColor: '#E93EFF',
        //Set transparency
        fillOpacity: 0.35
    });
    polygon.setMap(map);


    for (var i = 0; i < currentSaveData.length; i++) {
        //Set the temp position Lat and Lng from currentSaveData.
        tempLat += currentSaveData[i].lat;
        tempLng += currentSaveData[i].lng;
    }
    //Calculate the center position of the polygon.
    var latAve = tempLat / currentSaveData.length;
    var lngAve = tempLng / currentSaveData.length;
    //panTo the centre of our location
    map.panTo({
        lat: latAve,
        lng: lngAve
    });
}

//When click the botton,it will delete the corresponding viewPage and automatically back to the index page
function deleteIt() {
    var deletKey = localStorage.key(numOfRegion);
    //Also delete the sessionStorage data.
    sessionStorage.removeItem(settingKey);
    localStorage.removeItem(deletKey);
    location.href = 'index.html';
}

//Display the number of fences before the toggle.(0)
fenceNumRef = document.getElementById("numberOfFenceposts");
fenceNumRef.innerHTML = numFenceposts;

//Create a toggle botton to display the fence and make the fence disappear
function toggle() {

    //Set up the property for the lineSymbol
    var lineSymbol = {
        //Use Google Map API elements
        path: 'M 0,-0.07 0,0.07',
        //Set the transparency
        strokeOpacity: 1,
        scale: 4
    };

    //If there is no fence on the page, it will ask the user to input the value for interval in settingPage
    if (line === null) {
        //get the Value from setting page.
        var interval = gettingValue();
        //Calculate the fences repeat times.
        var repeatValue = String(interval * 100 / length) + '%'; //Calculate the repeat value
        //If the length > 0, it can display the number of the fences.
        if (length !== 0) {
            //Find the rounding number about the number of fences.
            answerNumFence = Math.round(length / interval);
            //Display the number of fences.
            fenceNumRef.innerHTML = answerNumFence;
            saveData.setNumOfFenceposts(answerNumFence);
        } else {
            //If length = 0, it will directly give the value 1 to the webpage
            fenceNumRef.innerHTML = 1;
            saveData.setNumOfFenceposts(1);
        }


        //Determine whether there is a settingKey in sessionStorage, and then drop fences if it exists
        var tempPostValue = Number(JSON.parse(sessionStorage.getItem(settingKey)));
        if (tempPostValue != 0) {

            //Show the fence on the map in the viewRegion page
            line = new google.maps.Polyline({
                //Display the fences in the correct path based on currentSaveData.
                path: currentSaveData,
                //Set transparency.
                strokeOpacity: 0,
                //Set the icon.
                icons: [{
                    icon: lineSymbol,
                    offset: '0',
                    //Set the repeat times.
                    repeat: repeatValue
                }],
                map: map
            })
            cornerPost(currentSaveData, lineSymbol); //Show the fences on every corner.
        }


    } else {

        //If the fence is already shown, reload the page
        location.reload();
    }
}

function cornerPost(currentPos, iconLine) { //Show the fences on every corner.
    var Post = null;
    for (var i = 0; i < currentPos.length; i++) {
        Post = new google.maps.Marker({
            //Set the icon
            icon: iconLine,
            //Set the corner position.
            position: new google.maps.LatLng(currentPos[i]),
            map: map
        })
    }

}

function settingValue() { //In settingPage, user can input the The distance between every two fences.
    var FenceValueRef = document.getElementById("FenceValue");
    var FenceNumber = Number(FenceValueRef.value);
    sessionStorage.setItem(settingKey, JSON.stringify(FenceNumber)); //put theuser input into the sessionStorage.
}

function resetSetting() { //If user input error, they can reset.
    sessionStorage.removeItem(settingKey);
    location.reload();
}

function gettingValue() { //Get the value from sessionStorage by settingKey.
    var loadValue = null;
    loadValue = Number(JSON.parse(sessionStorage.getItem(settingKey)));
    return loadValue;
}