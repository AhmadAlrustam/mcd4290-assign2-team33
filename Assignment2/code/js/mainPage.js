// Code for the main app page (Regions List).

// The following is sample code to demonstrate navigation.
// You need not use it for final app.

var viewPosition;
var tempSavePos; //Set an variable called tempSavePos to save the position from the local storage
var i = 0; //And set the initial value i and the stringValues
var stringValue = "viewRegion(0);";

//Check if the local storage can be used.If not, jump a window to let user know.
if (typeof(Storage) === "undefined") {
    displayMessage("localStorage is not supported by current browser.");
}


function viewRegion(regionIndex) {
    // Save the desired region to local storage so it can be accessed from view region page
    viewPosition = JSON.parse(localStorage.getItem(localStorage.key(regionIndex)));
    // ... and load the view region page.
    location.href = 'viewRegion.html';
    sessionStorage.setItem("Clicking", regionIndex); //Get the clicking position from the session storage
}


//Use the while loop to create a list based on the local storage
while (localStorage.getItem(localStorage.key(i)) !== null) {
    //Set the temp variable from localStorage.
    tempSavePos = localStorage.key(i);
    //Use Addlist() to add the new Region List
    AddList(stringValue, String('Region ' + (i + 1)), tempSavePos);
    i++;
    //Update the stringValue;
    stringValue = "viewRegion(" + String(i) + ");";
}

//Create a function which is used to add a new list into the index page when clicking the save botton
function AddList(viewRegionIndex, RegionName, Optional) {
    //Create object and use it with appendChild().
    var divOne = document.createElement('li');
    var spanOne = document.createElement('span');
    var spanTwo = document.createElement('span');
    var spanThree = document.createElement('span');
    //Specify the class name in html.
    spanOne.className = "mdl-list__item-primary-content";
    spanThree.className = "mdl-list__item-sub-title";
    divOne.className = "mdl-list__item mdl-list__item--two-line";
    divOne.setAttribute("onclick", viewRegionIndex); //Add attribute: clickable.
    document.getElementsByTagName('ul')[0].appendChild(divOne);
    //Add new node.
    divOne.appendChild(spanOne);
    spanOne.appendChild(spanTwo);
    spanOne.appendChild(spanThree);
    spanTwo.innerHTML = RegionName;
    spanThree.innerHTML = Optional;
}